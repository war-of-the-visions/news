import re
from urllib.request import urlopen
import json
import os
import urllib.parse

reNews = re.compile(r"data-tab=\"(.+?)\".+?>.+?<p>(.+?)<\/p>", flags=re.S)

reATitle = re.compile(r"<h1 id=\"article_title\">(.*?)</h1>")
reADate = re.compile(r"<time><span id=\"article_date\">(.+?)</span></time>")
reACategory = re.compile(r"<i id=\"article_category\" class=\"badge_info\">(.*?)</i>")

reImg = re.compile(r"<img src=\"(.+?)\".+?>")
reTitle = re.compile(r"<div class=\"text-title\">(.+?)</div>")

reTable = re.compile(r"<table class=\"table\">(.+?)<\/table>", flags=re.DOTALL)
reTableTitle = re.compile(
    r"<th class=\"table__cell--title\">(.+?)<\/th>", flags=re.DOTALL
)
reTableEntry = re.compile(r"<td>(.+?)<\/td>", flags=re.DOTALL)

reCodeBody = re.compile(r"\s*`\s*`\s*`\s*")  # fixes `` `

HOST_JP = "https://site.wotvffbe.com/"
HOST_GL = "https://site.na.wotvffbe.com/"  # 443


def download_news(host, language, path):
    os.makedirs(path, exist_ok=True)
    news_fp = os.path.join(path, "news.json")
    s_news = {}
    if os.path.isfile(news_fp):
        with open(news_fp, "rb") as f:
            s_news = json.loads(f.read())

    news = (
        urlopen(f"{host}whatsnew/list?page=1&category=info&platform=&lang={language}")
        .read()
        .decode("utf8")
    )

    new = False
    for id, title in reNews.findall(news)[::-1]:
        if id not in news:
            new = True

            print(id, title)
            detail = (
                urlopen(f"{host}whatsnew/detail?group_id={id}&lang={language}")
                .read()
                .decode("utf8")
            )
            # save images locally
            for image in reImg.findall(detail):
                print(image)
                fp = os.path.join(path, *image.split("/"))
                os.makedirs(os.path.dirname(fp), exist_ok=True)
                with open(fp, "wb") as f:
                    f.write(urlopen(host + urllib.parse.quote(image)).read())

        s_news[id] = {"title": title, "detail": detail}

    if new:
        with open(news_fp, "wb") as f:
            f.write(json.dumps(s_news, ensure_ascii=False).encode("utf8"))


if __name__ == "__main__":
    download_news(HOST_JP, "news_jp", "ja")
    download_news(HOST_GL, "news_gl", "en")
